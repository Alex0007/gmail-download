var CLIENT_ID = '463081282889-grejnlf9bfv3enlsk8n3jlabevj5hpgm.apps.googleusercontent.com'
var CLIENT_SECRET = 'ZI1xQrazHL1MHeoGg-bMD8wY'
var REDIRECT_URL = 'http://localhost'

process.env.MONGO_URL = 'mongodb://test:test@dogen.mongohq.com:10085/murzakaev-mails'

var mongoose = require('mongoose')
var readline = require('readline')
var Mail = require('./mail-schema')

var google = require('googleapis')
var async = require('neo-async')
var gmail = google.gmail('v1')
var OAuth2 = google.auth.OAuth2


var oauth2Client = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL)

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})



function saveToDb(obj, cb) {
    /*
    message.html
    message.date
    */

    var mail = new Mail(obj)
    mail.save(cb)

    // // TODO implement saving to somewhere here
    //
    // console.log('saveToDB obj', obj)
    // setTimeout(cb, 3000)
}

function getAccessToken(cb) {
    var token

    try {
        token = require('./token')
    } catch (e) {}

    if (token) {
        oauth2Client.setCredentials(token)
        cb && cb()
    } else {
        var url = oauth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: 'https://www.googleapis.com/auth/gmail.readonly'
        })

        console.log('Visit the url: ', url);
        rl.question('Paste the code here: ', function(code) {
            oauth2Client.getToken(code, function(err, token) {
                console.log('token', token)
                oauth2Client.setCredentials(token)
                cb && cb()
            })
        })
    }
}

function findMessages(from, cb) {

    var from = from || ''
    var messages = []
    var pageToken = undefined

    async.whilst(function() {
        return pageToken != ''
    }, function(cb) {
        gmail.users.messages.list({
            userId: 'me',
            auth: oauth2Client,
            q: 'from:' + from,
            pageToken: pageToken || ''
        }, function(err, resp) {
            pageToken = resp.nextPageToken || ''
            messages = messages.concat(resp.messages)
            cb()
        })
    }, function() {
        console.log('Found', messages.length.toString(), 'messages')
        cb(null, messages)
    })
}

function getMessage(id, cb) {
    gmail.users.messages.get({
        userId: 'me',
        id: id,
        auth: oauth2Client,
        format: 'full'
    }, function(err, resp) {

        var toReturn = {
            html: '',
            date: undefined
        }

        if (err) {
            cb && cb(err, null)
            return
        }

        if (resp.payload.body.data) { // getting toReturn.html
            toReturn.html = new Buffer(resp.payload.body.data, 'base64').toString()
        } else {

            var part = resp.payload.parts.forEach(function(part) {
                if (part.mimeType == 'text/html') {
                    toReturn.html += part.body.data
                }
                part.parts && part.parts.forEach(function(item) {
                    if (item.mimeType == 'text/html') {
                        toReturn.html += item.body.data
                    }
                })

            })

            toReturn.html = new Buffer(toReturn.html, 'base64').toString()

        }

        resp.payload.headers.forEach(function (header) { // getting toReturn.date
            if (header.name == 'Date') {
                toReturn.date = header.value
            }
        })

        cb(err, toReturn)

    })
}


async.waterfall([
        getAccessToken,
        function(cb) {
            rl.question('Download messages, sent from email: ', function(from) {
                cb(null, from)
            })
        },
        findMessages,
        function(messages, cb) { // saving to DB

            async.waterfall([
                function (cb) {
                    mongoose.connect(process.env.MONGO_URL)
                    mongoose.connection.on('open', function () {
                        cb(null)
                    })
                },
                function (cb) {
                    console.log('Connected to DB')

                    async.eachLimit(messages, 5, function(message, cb) {
                        getMessage(message.id, function(err, message) {
                            saveToDb(message, cb)
                        })
                    }, cb)
                }
            ], cb)

        }
    ],
    function() {
        console.log('done')
    })
