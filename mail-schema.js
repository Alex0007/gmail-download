var mongoose = require('mongoose') //.createConnection(process.env.MONGO_URL)

var MailSchema = new mongoose.Schema({
    html: String,
    date: {type: String, unique: true}
})

module.exports = mongoose.model('Mail', MailSchema)
